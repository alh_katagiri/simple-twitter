package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int id) {

    	Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, id);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    public void delete(int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(Integer userId, String startDate, String endDate){
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isBlank(startDate)) {
				startDate += " 00:00:00";
			} else {
				startDate = "2021/01/01 00:00:00";
			}
			if(!StringUtils.isBlank(endDate)) {
				endDate += " 23:59:59";
			} else {
				 Date date = new Date();
		         DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		         endDate = dateFormat.format(date);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, userId, startDate, endDate,  LIMIT_NUM);
			commit(connection);

			return messages;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
